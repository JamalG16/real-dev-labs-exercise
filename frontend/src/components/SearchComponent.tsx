import React, {useEffect, useState} from "react";
import {searchBooks} from "../api/gutendex";
import ReactPaginate from "react-paginate";
import {RotatingLines} from 'react-loader-spinner'

export const SearchComponent = () => {

    const [searchInput, setSearchInput] = useState("");
    const [books, setBooks] = useState([]);
    const [totalPages, setTotalPages] = useState(0);
    const [sort, setSort] = useState("");
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);

    useEffect(() => {
        handleSearch();
    }, [])

    const handleChange = (e: any) => {
        e.preventDefault();
        setSearchInput(e.target.value);
    };

    const handleSearch = () => {
        handleSearchWithPagination(1);
    }

    const handleSearchWithPagination = (page: number) => {
        setLoading(true);
        searchBooks(searchInput, sort, page ? page : 1).then((res) => {
            setTotalPages(res.count / 32);
            setBooks(res.results);
            setError(false);
        }).catch((e) => {
            console.log(e);
            setError(true);
        }).finally(() => {
            setLoading(false);
        });
    }

    const handleSort = () => {
        setLoading(true);
        if (sort === 'descending') {
            setSort('ascending');
        } else if (sort === 'ascending') {
            setSort('');
        } else {
            setSort('descending');
        }
        handleSearch();
    }

    const handlePageClick = (event : any) => {
        setLoading(true);
        handleSearchWithPagination(event.selected + 1);
    }

    const handleAddToShortlist = (book: any) => {
        const shortlist = JSON.parse(localStorage.getItem('shortlist') || "[]");
        shortlist.push(book);
        localStorage.setItem('shortlist', JSON.stringify(shortlist));
    }

    return (
        <>
            <h1>Search Books</h1>
            <RotatingLines
                strokeColor="grey"
                strokeWidth="5"
                animationDuration="0.75"
                width="96"
                visible={loading}
            />
            {!loading && error &&
                <>
                    <p>Error loading books, please try again.</p>
                </>}
            {!loading && !error &&
                <>
                    <input
                        type="text"
                        placeholder="Search here"
                        onChange={handleChange}
                        value={searchInput}
                    />
                    <button onClick={handleSearch}>
                        Search
                    </button>
                    <table style={{border: '1px solid black'}}>
                        <thead>
                            <tr style={{border: '1px solid black'}}>
                                <th style={{border: '1px solid black'}}>
                                    id
                                    <button type="button" onClick={handleSort}>
                                        Sort
                                    </button>
                                </th>
                                <th style={{border: '1px solid black'}}>title</th>
                                <th style={{border: '1px solid black'}}>author</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            books.map( (book : any , key) =>
                                    <tr key={key} style={{border: '1px solid black'}}>
                                    <td style={{border: '1px solid black'}}>{book.id }</td>
                                    <td style={{border: '1px solid black'}}>
                                        {book.title }
                                        <button onClick={() => handleAddToShortlist(book)}>
                                            Add to shortlist
                                        </button>
                                    </td>
                                        <td style={{border: '1px solid black'}}>{book.authors.map( (author : any) =>
                                            <p>{author.name}</p>)}</td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel="next >"
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={5}
                        pageCount={totalPages}
                        previousLabel="< previous"
                    />
                </>
            }
        </>
    );
}
