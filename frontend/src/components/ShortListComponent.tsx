import React, {useState} from "react";
import {reserveBook} from "../api/library";
import Modal from "react-modal";

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
    },
};

export const ShortListComponent = () => {

    const [showModal, setShowModal] = useState(false);
    const [username, setUsername] = useState("");
    const [book, setBook] = useState();

    const handleReserve = async () => {
        reserveBook(username, book).then((res) => {
            setShowModal(false);
        });
    }

    const handleReservationModal = (book: any) => {
        setBook(book);
        setUsername("");
        setShowModal(true);
    }

    const closeModal = () => {
        setShowModal(false);
    }

    const handleChange = (e: any) => {
        e.preventDefault();
        setUsername(e.target.value);
    };

    return (
        <>
            <Modal
                isOpen={showModal}
                onRequestClose={closeModal}
                contentLabel="Reservation"
            >
                <input
                    type="text"
                    placeholder="Username"
                    onChange={handleChange}
                    value={username}
                />
                <button onClick={handleReserve}>
                    Reserve
                </button>
                <button onClick={closeModal}>
                    Close
                </button>
            </Modal>
            <h1>Short list</h1>
            <table style={{border: '1px solid black'}}>
                <thead>
                <tr style={{border: '1px solid black'}}>
                    <th style={{border: '1px solid black'}}>id</th>
                    <th style={{border: '1px solid black'}}>title</th>
                    <th style={{border: '1px solid black'}}>author</th>
                </tr>
                </thead>
                <tbody>
                {
                    JSON.parse(localStorage.getItem("shortlist") || "[]").map( (book : any , key: React.Key | null | undefined) =>
                        <tr key={key} style={{border: '1px solid black'}}>
                            <td style={{border: '1px solid black'}}>{book.id }</td>
                            <td style={{border: '1px solid black'}}>
                                {book.title }
                                <button onClick={() => handleReservationModal(book)}>
                                    Reserve
                                </button>
                            </td>
                            <td style={{border: '1px solid black'}}>{book.authors.map( (author : any) =>
                                <p>{author.name}</p>)}</td>
                        </tr>
                    )
                }
                </tbody>
            </table>
        </>
    );
}
