import Navbar from "react-bootstrap/esm/Navbar";
import {Container, Nav, NavDropdown} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.css';
import { Routes, Route } from "react-router-dom";
import {SearchComponent} from "./components/SearchComponent";
import {ShortListComponent} from "./components/ShortListComponent";

export function App() {
  return (
      <>
          <Navbar bg="dark" variant="dark">
              <Container>
                  <Navbar.Brand>Library</Navbar.Brand>
                  <Nav className="me-auto">
                      <Nav.Link href="search">Search</Nav.Link>
                      <Nav.Link href="short-list">Shortlist</Nav.Link>
                  </Nav>
              </Container>
          </Navbar>
          <Routes>
              <Route path="/search" element={<SearchComponent />} />
              <Route path="/short-list" element={<ShortListComponent />} />
          </Routes>
      </>
  );
}
