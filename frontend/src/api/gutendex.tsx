import axios from "axios";

export async function searchBooks(query: string, sort: string, page: number) {
    try {
        const response = await axios.get('http://gutendex.com/books?search=' + encodeURI(query) + '&sort=' + sort + "&page=" + page);
        console.log('response  ', response)
        return response.data;
    } catch(error) {
        console.log(error);
        throw error;
    }

}
