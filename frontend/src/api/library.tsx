import axios from "axios";

export async function reserveBook(username: string, book: any) {
    try{
        const response = await axios.post('http://localhost:5000/api/book-reservations', {
            'username': username,
            'title': book.title,
            'author' : book.authors[0].name
        });
        console.log('response  ', response)
        return response;
    }catch(error) {
        console.log(error);
        return [];
    }

}
