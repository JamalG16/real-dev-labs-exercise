**To run the backend**

1. cd into /backend
2. npm install
3. npm run dev

**To run the frontend**
1. cd into /frontend
2. npm install
3. npm start


**Details About the ReactJS App**
1. User can search library for books.
2. Results are returned and paginated. The only field that can be sorted is API as I did not see any other sorting criteria specified by the Gutendex API.
Error state if GET api fails.
3. User can add books to their shortlist (no logic to filter out duplicates)
4. A tab where shortlist can be viewed
5. Shortlist is persisted in local storage
6. API call that reserves books from the shortlist

**Details About the Backend App**
1. POST /api/book-reservations. API accepts username, book title and author as params.
2. Username populated from the frontend APP is manually entered since we don't have any login.
3. Book and title and first author is saved, frontend is responsible for passing first author
4. Not done
5. Not done
6. GET /api/book-reservations?username. API returns books reserved by a user. 
