import {Entity, PrimaryGeneratedColumn, Column, BaseEntity} from "typeorm"

@Entity({name: 'reservations', database: 'library'})
export class BookReservation extends BaseEntity {

    @PrimaryGeneratedColumn({name: 'id'})
    id: number;

    @Column({name: 'username'})
    username: string;

    @Column({name: 'title'})
    title: string;

    @Column({name: 'author'})
    author: string;

    constructor(username: string, title: string, author: string) {
        super();
        this.username = username;
        this.title = title;
        this.author = author;
    }
}
