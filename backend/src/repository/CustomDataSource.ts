import { DataSource } from "typeorm"
import {BookReservation} from "../model/BookReservation";

export const CustomDataSource = new DataSource({
    type: "sqlite",
    database: "library",
    entities: [BookReservation],
    logging: true,
    synchronize: true
})
