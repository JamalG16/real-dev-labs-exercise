import {CustomDataSource} from './CustomDataSource';
import {Repository} from "typeorm";
import {BookReservation} from "../model/BookReservation";

export class BookReservationRepository {

    repository : Repository<BookReservation> = CustomDataSource.getRepository(BookReservation);

    public async save(bookReservation : BookReservation) : Promise<BookReservation> {
        return await this.repository.save(bookReservation);
    }

    public async getAllByUsername(username : any) : Promise<BookReservation[]> {
        return await this.repository.find({
            where: [{
                username : username
            }]
        });
    }
}
