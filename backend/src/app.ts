import express from 'express';
import {CustomDataSource} from "./repository/CustomDataSource";
import {BookReservationRepository} from "./repository/BookReservationRepository";
import {BookReservation} from "./model/BookReservation";
const app = express();
const port = 5000;
const bookReservationRepository = new BookReservationRepository();

app.use(express.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/api/book-reservations', async (req, res) => {
    try {
        console.log(req);
        const username: string = req.body['username'];
        const title: string = req.body['title'];
        const author: string = req.body['author'];
        await bookReservationRepository.save(new BookReservation(username, title, author)).then((_) => {
            res.status(204).send();
        });
    } catch (e : any) {
        console.log(e);
        res.status(500).send({error: "internal server error"});
    }
});

app.get('/api/book-reservations', async (req, res) => {
    try {
        const username = req.query.username;
        await bookReservationRepository.getAllByUsername(username).then((reservations: BookReservation[]) => {
            res.status(200).send({reservations});
        });
    } catch (e : any) {
        console.log(e);
        res.status(500).send({error: "internal server error"});
    }
});

CustomDataSource.initialize().then(async () => {
    app.listen(port, () => {
        return console.log(`Express is listening at http://localhost:${port}`);
    });
}).catch((e: any) => {
  console.log("Failed to initialize data source " + e);
})

